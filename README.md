smartics Integration for Piwik
==============================


## Overview

More than just powerful analytics in your team collaboration platform.

"Piwik is the leading open-source analytics platform that gives you more than
just powerful analytics."

This add-on provides macros to call the Piwik API from your Confluence pages easily.

  * Custom Variables
  * Document Properties


## Fork me!
Feel free to fork this project to add your ideas!

The add-on is licensed under [GNU GENERAL PUBLIC LICENSE, Version 3](http://www.gnu.org/licenses/).

This license applies only to the artifacts of this project.

Piwik is an internationally registered trademark, currently assigned to the
project’s founder and lead developer, Matthieu Aubry.

Piwik is the leading open-source analytics platform that gives you more
than just powerful analytics.

Piwik is Free Software released under the GPL 3.

For more information on Piwik: http://piwik.org/


## Documentation

For more information please visit (soon)

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/piwik)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-piwik)
