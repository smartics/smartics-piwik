﻿smartics Integration for Piwik
Integration of Piwik analytics into Confluence.

Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


..............................................................................

= Piwik

Piwik is an internationally registered trademark, currently assigned to the
project’s founder and lead developer, Matthieu Aubry.

Piwik is the leading open-source analytics platform that gives you more
than just powerful analytics.

Piwik is Free Software released under the GPL 3.

For more information on Piwik: http://piwik.org
