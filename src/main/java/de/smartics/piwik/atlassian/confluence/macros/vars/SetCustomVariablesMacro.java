/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.vars;

import static de.smartics.piwik.atlassian.confluence.macros.vars.Utils.readContextParameter;

import de.smartics.piwik.atlassian.confluence.macros.common.AbstractPiwikMacro;
import de.smartics.piwik.atlassian.confluence.macros.common.MacroUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Sets custom variables to the Piwik context.
 */
public class SetCustomVariablesMacro extends AbstractPiwikMacro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Provides access to the locale.
   */
  private final LocaleManager localeManager;

  /**
   * Provides access to localized information.
   */
  private final I18NBeanFactory i18nBeanFactory;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public SetCustomVariablesMacro(final LocaleManager localeManager,
      final I18NBeanFactory i18nBeanFactory) {
    this.localeManager = localeManager;
    this.i18nBeanFactory = i18nBeanFactory;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.RICH_TEXT;
  }

  @Override
  public OutputType getOutputType() {

    return OutputType.INLINE;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {
    try {
      final String varContext = readContextParameter(parameters);
      final CustomVariables customVariables =
          new CustomVariables(context, varContext);
      final TableParser parser = new TableParser(customVariables);
      parser.parse(body);

      if (customVariables.isEmpty()) {
        return StringUtils.EMPTY;
      }

      return customVariables.renderSeparateScript();
    } catch (final SAXException | IOException | DocumentException
        | ParserConfigurationException e) {

      final Locale locale =
          localeManager.getLocale(MacroUtils.getAuthenticatedUser());
      final I18NBean i18n = i18nBeanFactory.getI18NBean(locale);

      final String errorMessage = RenderUtils.blockError(i18n.getText(
          "de.smartics.piwik.error.cannotParseCustomVariablesTable",
          new Object[] {e.getMessage()}), "");
      return errorMessage;
    }
  }

  // --- object basics --------------------------------------------------------

}
