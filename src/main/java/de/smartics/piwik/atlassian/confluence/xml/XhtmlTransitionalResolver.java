/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.xml;

import com.atlassian.confluence.xml.XhtmlEntityResolver;
import com.atlassian.core.util.ClassLoaderUtils;

import org.apache.commons.io.IOUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

/**
 * Provides DTDs we use for parsing XHTML. Adds
 * <code>-//W3C//DTD XHTML 1.0 Transitional//EN</code> to be locally available.
 * <p>
 * As an extension to {@link XhtmlEntityResolver}, this implementation reuses
 * code from this class.
 * </p>
 */
public class XhtmlTransitionalResolver extends XhtmlEntityResolver {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Maps public entity ids to the resource that defines it.
   */
  private final Map<String, String> publicIdToEntityMap =
      new HashMap<String, String>(1);

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public XhtmlTransitionalResolver() {
    try {
      publicIdToEntityMap.put("-//W3C//DTD XHTML 1.0 Transitional//EN",
          readResource("xhtml1-transitional.dtd"));
    } catch (final IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Object resolveEntity(final String publicId, final String systemId,
      final String baseUri, final String namespace) throws XMLStreamException {
    final String resource = publicIdToEntityMap.get(publicId);

    if (resource == null) {
      return super.resolveEntity(publicId, systemId, baseUri, namespace);
    }

    return new ByteArrayInputStream(resource.getBytes());
  }

  @Override
  public InputSource resolveEntity(final String publicId, final String systemId)
      throws SAXException, IOException {
    final String resource = publicIdToEntityMap.get(publicId);

    if (resource == null) {
      return super.resolveEntity(publicId, systemId);
    }

    return new InputSource(new StringReader(resource));
  }

  private String readResource(final String name) throws IOException {
    final String resourceName = "de/smartics/piwik/xhtml/" + name;
    final InputStream input = ClassLoaderUtils.getResourceAsStream(resourceName,
        XhtmlTransitionalResolver.class);
    if (input == null) {
      throw new IOException("The resource " + resourceName
          + " has not been found on the classpath.");
    }

    try {
      final String resource = IOUtils.toString(input, "UTF-8");
      return resource;
    } finally {
      input.close();
    }
  }

  // --- object basics --------------------------------------------------------

}
