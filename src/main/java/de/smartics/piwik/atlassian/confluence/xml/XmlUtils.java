/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.xml;

/**
 * Utilities to parse XML.
 */
public class XmlUtils {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------
  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  private XmlUtils() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * WORKAROUND: Attributes are not properly separated by spaces.
   *
   * @param xhtmlFragment the fragment to fix.
   * @return the fixed fragment.
   */
  public static final String fixXhtmlFragment(final String xhtmlFragment) {
    final String fixedXhtmlFragment =
        xhtmlFragment.replace("'data-", "' data-");
    return fixedXhtmlFragment;
  }

  // --- object basics --------------------------------------------------------

}
