/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.document;

import de.smartics.piwik.atlassian.confluence.macros.common.AbstractPiwikMacro;
import de.smartics.piwik.atlassian.confluence.macros.common.MacroUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.RenderMode;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Exchanges the title of a Piwik document.
 */
public class DocumentPropertiesMacro extends AbstractPiwikMacro {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name of the parameter that stores the new title for the document.
   */
  private static final String PARAM_TITLE = "title";

  /**
   * The name of the parameter that stores the campaign this page is part of.
   */
  private static final String PARAM_CAMPAIGN = "campaign";

  /**
   * The name of the parameter that stores the campaign keyword this page is
   * associated with.
   */
  private static final String PARAM_CAMPAIGN_KEYWORD = "campaignKeyword";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public DocumentPropertiesMacro() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType() {

    return OutputType.INLINE;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {

    final DocumentProperties properties = new DocumentProperties(context);
    final String title = fetchNewTitle(parameters, context);
    properties.setTitle(title);

    final String campaign =
        MacroUtils.getParameter(parameters, PARAM_CAMPAIGN, null);
    if (StringUtils.isNotBlank(campaign)) {
      properties.setCampaign(campaign);
    }

    final String campaignKeyword =
        MacroUtils.getParameter(parameters, PARAM_CAMPAIGN_KEYWORD, null);
    if (StringUtils.isNotBlank(campaignKeyword)) {
      properties.setCampaignKeyword(campaignKeyword);
    }

    return properties.renderSeparateScript();
  }

  private String fetchNewTitle(final Map<String, String> parameters,
      final ConversionContext context) {
    String newTitle = MacroUtils.getParameter(parameters, PARAM_TITLE, null);
    if (StringUtils.isBlank(newTitle)) {
      newTitle = context.getEntity().getDisplayTitle();
    }

    return newTitle;
  }

  // --- object basics --------------------------------------------------------

}
