/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.common;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 *
 */
public abstract class PiwikRenderer implements Serializable {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The class version identifier.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Locates the script buffer in the conversation context.
   */
  private static final String KEY =
      "de.smartics.piwik.atlassian.confluence.macros.scriptBuffer";

  // --- members --------------------------------------------------------------

  protected final ConversionContext context;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected PiwikRenderer(final ConversionContext context) {
    this.context = context;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  protected static String escape(final String value) {
    return StringEscapeUtils
        .escapeEcmaScript(StringEscapeUtils.escapeHtml4(value));
  }

  public abstract void render(StringBuilder buffer);


  private void renderScriptStart(final StringBuilder buffer) {
    buffer.append("<script type='text/javascript'>\n");
    buffer.append("  var _paq = _paq || [];\n");
  }

  private void renderScriptEnd(final StringBuilder buffer) {
    buffer.append("</script>");
  }

  public String renderSeparateScript() {
    final StringBuilder buffer = new StringBuilder(255);
    renderScriptStart(buffer);
    render(buffer);
    renderScriptEnd(buffer);
    return buffer.toString();
  }


  public String renderScript() {
    StringBuilder buffer = (StringBuilder) context.getProperty(KEY);
    if (buffer != null) {
      renderScriptEnd(buffer);
      return buffer.toString();
    }

    return StringUtils.EMPTY;
  }

  public void appendToScript() {
    final StringBuilder buffer = fetchBuffer();
    render(buffer);
  }

  private StringBuilder fetchBuffer() {
    StringBuilder buffer = (StringBuilder) context.getProperty(KEY);
    if (buffer == null) {
      buffer = new StringBuilder(256);
      renderScriptStart(buffer);
      context.setProperty(KEY, buffer);
    }
    return buffer;
  }

  // --- object basics --------------------------------------------------------

}
