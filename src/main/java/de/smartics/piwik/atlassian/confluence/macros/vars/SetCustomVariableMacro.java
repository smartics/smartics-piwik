/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.vars;

import static de.smartics.piwik.atlassian.confluence.macros.vars.Utils.readContextParameter;

import de.smartics.piwik.atlassian.confluence.macros.common.AbstractPiwikMacro;
import de.smartics.piwik.atlassian.confluence.macros.common.MacroUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.RenderMode;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Sets a single custom variable to the Piwik context. This may be used more
 * quickly since no table has to be created.
 */
public class SetCustomVariableMacro extends AbstractPiwikMacro {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name of the parameter that stores the name of the custom variable.
   */
  private static final String PARAM_NAME = "name";

  /**
   * The name of the parameter that stores the value of the custom variable.
   */
  private static final String PARAM_VALUE = "value";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public SetCustomVariableMacro() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType() {

    return OutputType.INLINE;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext context) throws MacroExecutionException {
    final String name =
        MacroUtils.getParameter(parameters, PARAM_NAME, "Category");
    final String value = MacroUtils.getParameter(parameters, PARAM_VALUE, null);

    if (StringUtils.isBlank(name) || StringUtils.isBlank(value)) {
      return StringUtils.EMPTY;
    }

    final String varContext = readContextParameter(parameters);
    final CustomVariables customVariables = new CustomVariables(context, varContext);
    customVariables.add(name, value);

    return customVariables.renderSeparateScript();
  }

  // --- object basics --------------------------------------------------------

}
