/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.vars;

import de.smartics.piwik.atlassian.confluence.macros.common.MacroUtils;

import java.util.Map;

/**
 * Utils used in this package dealing with custom variables.
 */
final class Utils {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Defines the name of the parameter that defines the context of a custom
   * variable.
   */
  public static final String PARAM_CONTEXT = "context";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Constant class pattern.
   */
  private Utils() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  static String readContextParameter(final Map<String, String> parameters) {
    final String varContext =
        MacroUtils.getParameter(parameters, Utils.PARAM_CONTEXT, "page");
    return varContext;
  }

  // --- object basics --------------------------------------------------------

}
