/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.common;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * Base class for all Piwik macros.
 */
public abstract class AbstractPiwikMacro extends BaseMacro implements Macro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  protected AbstractPiwikMacro() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public boolean isInline() {
    return getOutputType() == OutputType.INLINE;
  }

  @Override
  public boolean hasBody() {
    return getBodyType() != Macro.BodyType.NONE;
  }

  // --- business -------------------------------------------------------------

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public String execute(final Map parameters, final String body,
      final RenderContext renderContext) throws MacroException {
    try {
      return execute(parameters, body,
          new DefaultConversionContext(renderContext));
    } catch (final MacroExecutionException e) {
      throw new MacroException(e);
    }
  }

  // --- object basics --------------------------------------------------------

}
