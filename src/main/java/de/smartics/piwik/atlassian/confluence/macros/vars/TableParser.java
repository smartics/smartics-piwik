/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.vars;

import de.smartics.piwik.atlassian.confluence.xml.XhtmlTransitionalResolver;
import de.smartics.piwik.atlassian.confluence.xml.XmlUtils;

import com.atlassian.confluence.content.render.xhtml.Namespace;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.xml.XMLEntityResolver;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.Text;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Helper to do the actual HTML table parsing.
 */
final class TableParser {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG = LoggerFactory.getLogger(TableParser.class);

  /**
   * The factory to create XML parsers.
   */
  private static final SAXParserFactory XML_FACTORY =
      SAXParserFactory.newInstance();

  /**
   * The namespace prefix to construct an XML document from the passed in body.
   */
  private static final String DEFAULT_NAMESPACE_PREFIX = "xml";

  /**
   * The XPath expression to grab the first HTML table from the passed in page
   * fragment.
   */
  private static final String XPATH_TBODY = "//" + DEFAULT_NAMESPACE_PREFIX
      + ":table/" + DEFAULT_NAMESPACE_PREFIX + ":tbody[1]";

  /**
   * The namespace map used for parsing.
   */
  private static final Map<String, String> NAMESPACE_MAP;

  // --- members --------------------------------------------------------------

  private final XMLEntityResolver resolver = new XhtmlTransitionalResolver();

  private final CustomVariables builder;

  // ****************************** Initializer *******************************

  static {
    NAMESPACE_MAP = new ConcurrentHashMap<String, String>(
        XhtmlConstants.STORAGE_NAMESPACES.size());
    for (final Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES) {
      NAMESPACE_MAP.put(namespace.getPrefix() != null ? namespace.getPrefix()
          : DEFAULT_NAMESPACE_PREFIX, namespace.getUri());
    }
  }

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  TableParser(final CustomVariables builder) {
    this.builder = builder;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void parse(final String pageBody) throws IOException, DocumentException,
      ParserConfigurationException, SAXException {
    final Document macroBodyDoc = createXmlDocument(pageBody);

    final XPath xpath = createXPath(macroBodyDoc, XPATH_TBODY);
    final Element rootElement = (Element) xpath.selectSingleNode(macroBodyDoc);
    if (rootElement == null) {
      return;
    }

    final List<Element> rowElements = fetchRows(rootElement);
    if (rowElements != null) {
      loadDocumentPropertiesFromTableRows(rowElements);
    }
  }

  private static XPath createXPath(final Document macroBodyDoc,
      final String xPath) {
    final XPath xpath = macroBodyDoc.createXPath(xPath);
    xpath.setNamespaceURIs(NAMESPACE_MAP);
    return xpath;
  }

  @SuppressWarnings("unchecked")
  private List<Element> fetchRows(final Element rootElement) {
    return rootElement.elements("tr");
  }

  private Document createXmlDocument(
      final String xhtmlFragmentFromConfluencePage)
          throws DocumentException, UnsupportedEncodingException,
          ParserConfigurationException, SAXException {
    final String xmlDocumentString =
        createXmlDocumentString(xhtmlFragmentFromConfluencePage);
    final ByteArrayInputStream input =
        new ByteArrayInputStream(xmlDocumentString.getBytes("UTF-8"));
    try {
      final SAXParser saxParser = XML_FACTORY.newSAXParser();
      final SAXReader saxReader =
          new SAXReader(saxParser.getXMLReader(), false);
      saxReader.setFeature(
          "http://apache.org/xml/features/validation/unparsed-entity-checking",
          false);
      saxReader.setFeature(
          "http://apache.org/xml/features/nonvalidating/load-external-dtd",
          true);
      saxReader.setEntityResolver(resolver);
      final Document document = saxReader.read(input);

      return document;
    } finally {
      IOUtils.closeQuietly(input);
    }
  }

  private String createXmlDocumentString(final String xmlFragmentString) {
    final StringBuilder buffer =
        new StringBuilder(xmlFragmentString.length() + 2048);
    buffer.append(
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\""
            + " \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<xhtml");

    for (final Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES) {
      buffer.append(" xmlns");
      if (!namespace.isDefaultNamespace()) {
        buffer.append(":").append(namespace.getPrefix());
      }
      buffer.append("=\"").append(namespace.getUri()).append("\"");
    }

    final String fixedXmlFragmentString =
        XmlUtils.fixXhtmlFragment(xmlFragmentString);

    buffer.append(">").append(fixedXmlFragmentString).append("</xhtml>");
    return buffer.toString();
  }

  @SuppressWarnings("unchecked")
  private void loadDocumentPropertiesFromTableRows(
      final List<Element> rowElements) throws IOException {
    for (final Element rowElement : rowElements) {
      final List<Element> tds = rowElement.elements("td");
      if (tds.size() == 2) {
        final String value = getInnerHtml(tds.get(1));
        if (!isBlankValue(value)) {
          final Element nameElement = tds.get(0);
          final String name = nameElement.getTextTrim();
          builder.add(name, value);
        }
      } else {
        if (LOG.isDebugEnabled()) {
          if (rowElement.elements("th").isEmpty()) {
            LOG.debug(
                "Skipping illegal custom variable pair. Two values are required, but found {}.",
                tds.size());
          }
        }
      }
    }
  }

  private static final boolean isBlankValue(final String value) {
    return StringUtils.isBlank(value)
        || (value.length() == 1 && 160 == value.charAt(0))
        || "&nbsp;".equals(value) || containsOnlyWhitespaceTags(value);
  }

  private static boolean containsOnlyWhitespaceTags(String value) {
    final String testValue = value.replaceAll("<br>", "")
        .replaceAll("</br>", "").replaceAll("<br/>", "")
        .replaceAll(
            "<br xmlns=\"http://www.w3.org/1999/xhtml\" clear=\"none\"/>", "")
        .replaceAll(
            "<br xmlns=\"http://www.w3.org/1999/xhtml\" clear=\"none\">", "");
    return StringUtils.isBlank(testValue);
  }

  @SuppressWarnings("unchecked")
  private String getInnerHtml(final Element element) throws IOException {
    if (element == null) {
      return StringUtils.EMPTY;
    }

    if (element.isTextOnly()) {
      return StringEscapeUtils.escapeHtml4(element.getText());
    }

    final Writer writer = new StringWriter();

    final Iterator<Node> iterator = element.nodeIterator();
    while (iterator.hasNext()) {
      final Node node = iterator.next();
      if (node instanceof Text) {
        final Text text = (Text) node;
        final String content = text.getText();
        text.setText(StringEscapeUtils.escapeHtml4(content));
      }
      node.write(writer);
    }

    return writer.toString();
  }

  // --- object basics --------------------------------------------------------

}
