/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.vars;

import de.smartics.piwik.atlassian.confluence.macros.common.PiwikRenderer;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Container for Piwik custom variables.
 */
final class CustomVariables extends PiwikRenderer {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The class version identifier.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(CustomVariables.class);

  // --- members --------------------------------------------------------------

  private final Map<String, String> variables = new LinkedHashMap<>();

  private final String page;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  CustomVariables(final ConversionContext context, final String page) {
    super(context);
    this.page = page;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void add(final String name, final String value) {
    if (StringUtils.isNotBlank(name) || value != null) {
      variables.put(name, value);
    } else {
      if (LOG.isDebugEnabled()) {
        LOG.debug(
            "Skipping customer variable with {}='{}'."
                + " Non-blank name and non-null value is required.",
            name, value);
      }
    }
  }

  boolean isEmpty() {
    return variables.isEmpty();
  }

  public void render(final StringBuilder buffer) {
    int counter = 0;
    for (final Entry<String, String> entry : variables.entrySet()) {
      final String name = escape(entry.getKey());
      final String value = escape(entry.getValue());
      buffer.append("  _paq.push(['setCustomVariable', '").append(++counter)
          .append("','").append(name).append("','").append(value).append("','")
          .append(page).append("']);\n");
    }
  }

  // --- object basics --------------------------------------------------------

}
