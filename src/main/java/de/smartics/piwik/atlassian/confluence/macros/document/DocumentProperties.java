/*
 * smartics Integration for Piwik
 * Integration of Piwik analytics into Confluence.
 *
 * Copyright (C) 2015-2023 smartics, Kronseder & Reiner GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ............................................................................
 * Piwik is an internationally registered trademark, currently assigned to the
 * project's founder and lead developer, Matthieu Aubry.
 */
package de.smartics.piwik.atlassian.confluence.macros.document;

import de.smartics.piwik.atlassian.confluence.macros.common.PiwikRenderer;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Renderer for the document title method.
 */
final class DocumentProperties extends PiwikRenderer {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The class version identifier.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(DocumentProperties.class);

  // --- members --------------------------------------------------------------

  private String title;

  private String campaign;

  private String campaignKeyword;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  DocumentProperties(final ConversionContext context) {
    super(context);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public void setTitle(final String title) {
    this.title = title;
  }


  public void setCampaign(final String campaign) {
    this.campaign = campaign;
  }

  public void setCampaignKeyword(final String campaignKeyword) {
    this.campaignKeyword = campaignKeyword;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void render(final StringBuilder buffer) {
    if (StringUtils.isNotBlank(title)) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Setting title of page '{}' to '{}'.",
            context.getEntity().getDisplayTitle(), title);
      }
      buffer.append("  _paq.push(['setDocumentTitle', '").append(escape(title))
          .append("']);\n");
    }

    if (StringUtils.isNotBlank(campaign)) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Setting campaign of page '{}' to '{}'.",
            context.getEntity().getDisplayTitle(), campaign);
      }
      buffer.append("  _paq.push(['setCampaignNameKey', '")
          .append(escape(campaign)).append("']);\n");
    }

    if (StringUtils.isNotBlank(campaignKeyword)) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Setting campaign keyword of page '{}' to '{}'.",
            context.getEntity().getDisplayTitle(), campaignKeyword);
      }
      buffer.append("  _paq.push(['setCampaignKeywordKey', '")
          .append(escape(campaignKeyword)).append("']);\n");
    }
  }

  // --- object basics --------------------------------------------------------

}
